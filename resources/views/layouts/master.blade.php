<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>AdminLTE 3 | Dashboard 3</title>

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="/css/app.css">
    <!-- Google Font: Source Sans Pro -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper" id="app">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white border-bottom navbar-dark bg-primary">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="index3.html" class="nav-link">Home</a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="#" class="nav-link">Contact</a>
            </li>
        </ul>

        <!-- SEARCH FORM -->
        <form class="form-inline ml-3">
            <div class="input-group input-group-sm">
                <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-navbar" type="submit">
                        <i class="fas fa-search"></i>
                    </button>
                </div>
            </div>
        </form>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar elevation-4 sidebar-light-primary">
        <!-- Brand Logo -->
        <a href="index3.html" class="brand-link text-center">
            <img src="http://13.234.163.240/logo.png" width="89" height="25" alt="Modulr Logo" class="brand-text">
            {{--            <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"--}}
            {{--                 style="opacity: .8">--}}
            {{--            <span class="brand-text font-weight-light">Workonic</span>--}}
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                    <img src="http://13.234.163.240/defaultAvatar.png" class="img-circle elevation-2" alt="User Image">
                </div>
                <div class="info">
                    <a href="#" class="d-block">{{Auth::user()->name}}</a>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                    data-accordion="false">
                    <!-- Add icons to the links using the .nav-icon class
                         with font-awesome or any other icon font library -->
                    <li class="nav-item">
                        <router-link to="/dashboard" class="nav-link ">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>
                               Dashboard
                            </p>
                        </router-link>
                    </li>
                    <li class="nav-item">
                        <router-link to="/job_area" class="nav-link">
                            <i class="nav-icon fas fa-th"></i>
                            <p>
                                Job Area
                            </p>
                        </router-link>
                    </li>
                    <li class="nav-item">
                        <router-link to="/reporting" class="nav-link">
                            <i class="nav-icon fas fa-copy"></i>
                            <p>
                                Reporting
                            </p>
                        </router-link>
                    </li>
                    <li class="nav-item">
                        <router-link to="/calender" class="nav-link">
                            <i class="nav-icon fas fa-chart-pie"></i>
                            <p>
                                Calender
                            </p>
                        </router-link>
                    </li>
                    <li class="nav-item">
                        <router-link to="/upload_job" class="nav-link">
                            <i class="nav-icon fa-chart-pie"></i>
                            <p>
                                Upload Job
                            </p>
                        </router-link>
                    </li>
                    <li class="nav-item">
                        <router-link to="/job_area_admin" class="nav-link">
                            <i class="nav-icon fa-chart-pie"></i>
                            <p>
                                Job Area Admin
                            </p>
                        </router-link>
                    </li>
                    <li class="nav-item">
                        <router-link to="/users" class="nav-link">
                            <i class="nav-icon fa-chart-pie"></i>
                            <p>
                                Users

                            </p>
                        </router-link>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                              document.getElementById('logout-form').submit();">
                            <i class="nav-icon fa fa-power-off red"></i>
                            <p>
                                {{ __('Logout') }}
                            </p>
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
    {{--        <div class="content-header">--}}
    {{--            <div class="container-fluid">--}}
    {{--                <div class="row mb-2">--}}
    {{--                    <div class="col-sm-6">--}}
    {{--                        <h1 class="m-0 text-dark">Dashboard v3</h1>--}}
    {{--                    </div><!-- /.col -->--}}
    {{--                    <div class="col-sm-6">--}}
    {{--                        <ol class="breadcrumb float-sm-right">--}}
    {{--                            <li class="breadcrumb-item"><a href="#">Home</a></li>--}}
    {{--                            <li class="breadcrumb-item active">Dashboard v3</li>--}}
    {{--                        </ol>--}}
    {{--                    </div><!-- /.col -->--}}
    {{--                </div><!-- /.row -->--}}
    {{--            </div><!-- /.container-fluid -->--}}
    {{--        </div>--}}
    <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <router-view></router-view>
{{--                            <div class="row">--}}
{{--                                <div class="col-lg-6">--}}
{{--                                    <div class="card">--}}
{{--                                        <div class="card-header no-border">--}}
{{--                                            <div class="d-flex justify-content-between">--}}
{{--                                                <h3 class="card-title">Online Store Visitors</h3>--}}
{{--                                                <a href="javascript:void(0);">View Report</a>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="card-body">--}}
{{--                                            <div class="d-flex">--}}
{{--                                                <p class="d-flex flex-column">--}}
{{--                                                    <span class="text-bold text-lg">820</span>--}}
{{--                                                    <span>Visitors Over Time</span>--}}
{{--                                                </p>--}}
{{--                                                <p class="ml-auto d-flex flex-column text-right">--}}
{{--                                <span class="text-success">--}}
{{--                                  <i class="fas fa-arrow-up"></i> 12.5%--}}
{{--                                </span>--}}
{{--                                                    <span class="text-muted">Since last week</span>--}}
{{--                                                </p>--}}
{{--                                            </div>--}}
{{--                                            <!-- /.d-flex -->--}}

{{--                                            <div class="position-relative mb-4">--}}
{{--                                                <canvas id="visitors-chart" height="200"></canvas>--}}
{{--                                            </div>--}}

{{--                                            <div class="d-flex flex-row justify-content-end">--}}
{{--                              <span class="mr-2">--}}
{{--                                <i class="fas fa-square text-primary"></i> This Week--}}
{{--                              </span>--}}

{{--                                                <span>--}}
{{--                                <i class="fas fa-square text-gray"></i> Last Week--}}
{{--                              </span>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <!-- /.card -->--}}

{{--                                    <div class="card">--}}
{{--                                        <div class="card-header no-border">--}}
{{--                                            <h3 class="card-title">Products</h3>--}}
{{--                                            <div class="card-tools">--}}
{{--                                                <a href="#" class="btn btn-tool btn-sm">--}}
{{--                                                    <i class="fas fa-download"></i>--}}
{{--                                                </a>--}}
{{--                                                <a href="#" class="btn btn-tool btn-sm">--}}
{{--                                                    <i class="fas fa-bars"></i>--}}
{{--                                                </a>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="card-body p-0">--}}
{{--                                            <table class="table table-striped table-valign-middle">--}}
{{--                                                <thead>--}}
{{--                                                <tr>--}}
{{--                                                    <th>Product</th>--}}
{{--                                                    <th>Price</th>--}}
{{--                                                    <th>Sales</th>--}}
{{--                                                    <th>More</th>--}}
{{--                                                </tr>--}}
{{--                                                </thead>--}}
{{--                                                <tbody>--}}
{{--                                                <tr>--}}
{{--                                                    <td>--}}
{{--                                                        <img src="dist/img/default-150x150.png" alt="Product 1" class="img-circle img-size-32 mr-2">--}}
{{--                                                        Some Product--}}
{{--                                                    </td>--}}
{{--                                                    <td>$13 USD</td>--}}
{{--                                                    <td>--}}
{{--                                                        <small class="text-success mr-1">--}}
{{--                                                            <i class="fas fa-arrow-up"></i>--}}
{{--                                                            12%--}}
{{--                                                        </small>--}}
{{--                                                        12,000 Sold--}}
{{--                                                    </td>--}}
{{--                                                    <td>--}}
{{--                                                        <a href="#" class="text-muted">--}}
{{--                                                            <i class="fas fa-search"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </td>--}}
{{--                                                </tr>--}}
{{--                                                <tr>--}}
{{--                                                    <td>--}}
{{--                                                        <img src="dist/img/default-150x150.png" alt="Product 1" class="img-circle img-size-32 mr-2">--}}
{{--                                                        Another Product--}}
{{--                                                    </td>--}}
{{--                                                    <td>$29 USD</td>--}}
{{--                                                    <td>--}}
{{--                                                        <small class="text-warning mr-1">--}}
{{--                                                            <i class="fas fa-arrow-down"></i>--}}
{{--                                                            0.5%--}}
{{--                                                        </small>--}}
{{--                                                        123,234 Sold--}}
{{--                                                    </td>--}}
{{--                                                    <td>--}}
{{--                                                        <a href="#" class="text-muted">--}}
{{--                                                            <i class="fas fa-search"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </td>--}}
{{--                                                </tr>--}}
{{--                                                <tr>--}}
{{--                                                    <td>--}}
{{--                                                        <img src="dist/img/default-150x150.png" alt="Product 1" class="img-circle img-size-32 mr-2">--}}
{{--                                                        Amazing Product--}}
{{--                                                    </td>--}}
{{--                                                    <td>$1,230 USD</td>--}}
{{--                                                    <td>--}}
{{--                                                        <small class="text-danger mr-1">--}}
{{--                                                            <i class="fas fa-arrow-down"></i>--}}
{{--                                                            3%--}}
{{--                                                        </small>--}}
{{--                                                        198 Sold--}}
{{--                                                    </td>--}}
{{--                                                    <td>--}}
{{--                                                        <a href="#" class="text-muted">--}}
{{--                                                            <i class="fas fa-search"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </td>--}}
{{--                                                </tr>--}}
{{--                                                <tr>--}}
{{--                                                    <td>--}}
{{--                                                        <img src="dist/img/default-150x150.png" alt="Product 1" class="img-circle img-size-32 mr-2">--}}
{{--                                                        Perfect Item--}}
{{--                                                        <span class="badge bg-danger">NEW</span>--}}
{{--                                                    </td>--}}
{{--                                                    <td>$199 USD</td>--}}
{{--                                                    <td>--}}
{{--                                                        <small class="text-success mr-1">--}}
{{--                                                            <i class="fas fa-arrow-up"></i>--}}
{{--                                                            63%--}}
{{--                                                        </small>--}}
{{--                                                        87 Sold--}}
{{--                                                    </td>--}}
{{--                                                    <td>--}}
{{--                                                        <a href="#" class="text-muted">--}}
{{--                                                            <i class="fas fa-search"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </td>--}}
{{--                                                </tr>--}}
{{--                                                </tbody>--}}
{{--                                            </table>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <!-- /.card -->--}}
{{--                                </div>--}}
{{--                                <!-- /.col-md-6 -->--}}
{{--                                <div class="col-lg-6">--}}
{{--                                    <div class="card">--}}
{{--                                        <div class="card-header no-border">--}}
{{--                                            <div class="d-flex justify-content-between">--}}
{{--                                                <h3 class="card-title">Sales</h3>--}}
{{--                                                <a href="javascript:void(0);">View Report</a>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="card-body">--}}
{{--                                            <div class="d-flex">--}}
{{--                                                <p class="d-flex flex-column">--}}
{{--                                                    <span class="text-bold text-lg">$18,230.00</span>--}}
{{--                                                    <span>Sales Over Time</span>--}}
{{--                                                </p>--}}
{{--                                                <p class="ml-auto d-flex flex-column text-right">--}}
{{--                                <span class="text-success">--}}
{{--                                  <i class="fas fa-arrow-up"></i> 33.1%--}}
{{--                                </span>--}}
{{--                                                    <span class="text-muted">Since last month</span>--}}
{{--                                                </p>--}}
{{--                                            </div>--}}
{{--                                            <!-- /.d-flex -->--}}

{{--                                            <div class="position-relative mb-4">--}}
{{--                                                <canvas id="sales-chart" height="200"></canvas>--}}
{{--                                            </div>--}}

{{--                                            <div class="d-flex flex-row justify-content-end">--}}
{{--                              <span class="mr-2">--}}
{{--                                <i class="fas fa-square text-primary"></i> This year--}}
{{--                              </span>--}}

{{--                                                <span>--}}
{{--                                <i class="fas fa-square text-gray"></i> Last year--}}
{{--                              </span>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <!-- /.card -->--}}

{{--                                    <div class="card">--}}
{{--                                        <div class="card-header no-border">--}}
{{--                                            <h3 class="card-title">Online Store Overview</h3>--}}
{{--                                            <div class="card-tools">--}}
{{--                                                <a href="#" class="btn btn-sm btn-tool">--}}
{{--                                                    <i class="fas fa-download"></i>--}}
{{--                                                </a>--}}
{{--                                                <a href="#" class="btn btn-sm btn-tool">--}}
{{--                                                    <i class="fas fa-bars"></i>--}}
{{--                                                </a>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="card-body">--}}
{{--                                            <div class="d-flex justify-content-between align-items-center border-bottom mb-3">--}}
{{--                                                <p class="text-success text-xl">--}}
{{--                                                    <i class="ion ion-ios-refresh-empty"></i>--}}
{{--                                                </p>--}}
{{--                                                <p class="d-flex flex-column text-right">--}}
{{--                                <span class="font-weight-bold">--}}
{{--                                  <i class="ion ion-android-arrow-up text-success"></i> 12%--}}
{{--                                </span>--}}
{{--                                                    <span class="text-muted">CONVERSION RATE</span>--}}
{{--                                                </p>--}}
{{--                                            </div>--}}
{{--                                            <!-- /.d-flex -->--}}
{{--                                            <div class="d-flex justify-content-between align-items-center border-bottom mb-3">--}}
{{--                                                <p class="text-warning text-xl">--}}
{{--                                                    <i class="ion ion-ios-cart-outline"></i>--}}
{{--                                                </p>--}}
{{--                                                <p class="d-flex flex-column text-right">--}}
{{--                                <span class="font-weight-bold">--}}
{{--                                  <i class="ion ion-android-arrow-up text-warning"></i> 0.8%--}}
{{--                                </span>--}}
{{--                                                    <span class="text-muted">SALES RATE</span>--}}
{{--                                                </p>--}}
{{--                                            </div>--}}
{{--                                            <!-- /.d-flex -->--}}
{{--                                            <div class="d-flex justify-content-between align-items-center mb-0">--}}
{{--                                                <p class="text-danger text-xl">--}}
{{--                                                    <i class="ion ion-ios-people-outline"></i>--}}
{{--                                                </p>--}}
{{--                                                <p class="d-flex flex-column text-right">--}}
{{--                                <span class="font-weight-bold">--}}
{{--                                  <i class="ion ion-android-arrow-down text-danger"></i> 1%--}}
{{--                                </span>--}}
{{--                                                    <span class="text-muted">REGISTRATION RATE</span>--}}
{{--                                                </p>--}}
{{--                                            </div>--}}
{{--                                            <!-- /.d-flex -->--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <!-- /.col-md-6 -->--}}
{{--                            </div>--}}
            <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->

    <!-- Main Footer -->
    <footer class="main-footer">
        <strong>Copyright &copy; 2014-2018 <a href="http://adminlte.io">AdminLTE.io</a>.</strong>
        All rights reserved.
        <div class="float-right d-none d-sm-inline-block">
            <b>Version</b> 3.0.0-beta.1
        </div>
    </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="/js/app.js"></script>
</body>
</html>
